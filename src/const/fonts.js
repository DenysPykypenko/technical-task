import {Platform} from 'react-native';
const isWEB = Platform.OS === 'web';

const FONTS_NATIVE = {
  regular: {fontFamily: 'Roboto-Regular'},
  bold: {fontFamily: 'Roboto-Bold'},
};

const FONTS_WEB = {
  regular: `@font-face {
    src: url("/public/fonts/Roboto-Regular.ttf");
    font-family: Roboto-Regular;
  }`,
  bold: `@font-face {
    src: url("/public/fonts/Roboto-Bold.ttf");
    font-family: Roboto-Bold;
  }`,
};

export default isWEB ? FONTS_WEB : FONTS_NATIVE;

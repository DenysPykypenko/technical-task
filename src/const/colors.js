const COLORS = {
  white: '#fff',
  gray: '#e4e4e4',
  text: '#555555',
  label: '#42454A',
  blue: '#00AFFF',
};

export default COLORS;

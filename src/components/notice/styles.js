import {StyleSheet} from 'react-native';
import COLORS from '../../const/colors';
import FONTS from '../../const/fonts';

export default StyleSheet.create({
  container: {
    maxWidth: 334,
    marginVertical: 18,
  },
  title: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 16,
    lineHeight: 22,
    letterSpacing: 0.08,
    color: COLORS.text,
    ...FONTS.bold,
  },
  message: {
    textAlign: 'center',
    fontSize: 16,
    lineHeight: 22,
    letterSpacing: -0.16,
    color: COLORS.text,
    ...FONTS.regular,
  },
});

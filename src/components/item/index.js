import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import styles from './styles';

const Item = ({margin, index, label, onPress}) => {
  return (
    <TouchableOpacity
      style={[styles.container, margin ? styles.margin : {}]}
      activeOpacity={0.9}
      onPress={() => onPress(index, label)}
    >
      <View style={styles.left}>
        <Text style={styles.textIndex}>{index}</Text>
      </View>
      <View style={styles.right}>
        <Text style={styles.textLabel}>{label}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default Item;

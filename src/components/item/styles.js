import {StyleSheet} from 'react-native';
import COLORS from '../../const/colors';
import FONTS from '../../const/fonts';

const radius = 80;
export default StyleSheet.create({
  margin: {
    marginLeft: 8,
  },
  container: {
    flex: 1,
    flexDirection: 'row',
    width: 160,
    height: 36,
    borderColor: COLORS.blue,
    borderRadius: radius,
    marginBottom: 8,
  },
  left: {
    width: 28,
    height: 36,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.blue,
    borderColor: COLORS.blue,
    borderTopStartRadius: radius,
    borderBottomStartRadius: radius,
  },
  right: {
    flexGrow: 1,
    padding: 8,
    alignItems: 'flex-start',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: COLORS.blue,
    borderTopEndRadius: radius,
    borderBottomEndRadius: radius,
  },
  textIndex: {
    fontSize: 16,
    letterSpacing: -0.32,
    color: COLORS.white,
    ...FONTS.regular,
  },
  textLabel: {
    fontSize: 16,
    letterSpacing: 0,
    color: COLORS.label,
    ...FONTS.regular,
  },
});

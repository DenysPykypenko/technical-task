import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import styles from './styles';

const Link = ({label, onPress}) => {
  return (
    <TouchableOpacity
      style={styles.container}
      activeOpacity={0.5}
      onPress={onPress}
    >
      <Text style={styles.label}>{label}</Text>
    </TouchableOpacity>
  );
};

export default Link;

import {StyleSheet} from 'react-native';
import COLORS from '../../const/colors';
import FONTS from '../../const/fonts';

export default StyleSheet.create({
  container: {
    width: '100%',
    height: 44,
    alignItems: 'center',
    justifyContent: 'center',
  },
  label: {
    textDecorationLine: 'underline',
    textAlign: 'center',
    fontSize: 15,
    lineHeight: 30,
    color: COLORS.blue,
    ...FONTS.regular,
  },
});

import {StyleSheet} from 'react-native';
import COLORS from '../../const/colors';
import FONTS from '../../const/fonts';

export default StyleSheet.create({
  container: {
    width: '100%',
    height: 44,
    borderRadius: 44 / 2,
    backgroundColor: COLORS.blue,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 16,
  },
  label: {
    textAlign: 'center',
    fontSize: 16,
    fontWeight: 'bold',
    color: COLORS.white,
    ...FONTS.regular,
  },
});

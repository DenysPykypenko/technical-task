import React from 'react';
import {FlatList} from 'react-native';
import Item from '../item';
import styles from './styles';

const items = [
  {index: 1, label: 'bike'},
  {index: 2, label: 'dark'},
  {index: 3, label: 'jumping'},
  {index: 4, label: 'cave'},
  {index: 5, label: 'rooms'},
  {index: 6, label: 'socks'},
  {index: 7, label: 'dove'},
  {index: 8, label: 'orange'},
  {index: 9, label: 'bottle'},
  {index: 10, label: 'blanket'},
  {index: 11, label: 'salt'},
  {index: 12, label: 'bug'},
];

const List = ({onItemPress}) => {
  return (
    <FlatList
      style={styles.container}
      scrollEnabled={false}
      contentContainerStyle={styles.content}
      showsHorizontalScrollIndicator={false}
      showsVerticalScrollIndicator={false}
      data={items}
      keyExtractor={item => item.index}
      numColumns={2}
      renderItem={item => (
        <Item
          margin={item.index % 2 !== 0}
          index={item.item.index}
          label={item.item.label}
          onPress={onItemPress}
        />
      )}
    />
  );
};

export default List;

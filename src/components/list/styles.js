import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    minHeight: 260,
    marginBottom: 16,
  },
  content: {
    paddingVertical: 4,
  },
});

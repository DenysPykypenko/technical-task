import {StyleSheet} from 'react-native';
import COLORS from './const/colors';

export default StyleSheet.create({
  safeArea: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.white,
  },
  scrollView: {
    flex: 1,
    width: 334,
  },
  bottom: {
    height: 70,
    width: 334,
    justifyContent: 'flex-end',
  },
});

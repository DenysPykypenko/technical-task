import React from 'react';
import {SafeAreaView, ScrollView, View} from 'react-native';
import List from './components/list';
import Notice from './components/notice';
import Button from './components/button';
import Link from './components/link';
import styles from './styles';

const App = () => {
  return (
    <SafeAreaView style={styles.safeArea}>
      <ScrollView
        style={styles.scrollView}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
      >
        <Notice
          title={'please save your 12-word pass phrase'}
          message={
            'and keep it in a secure location\nso you can recover your wallet anytime'
          }
        />

        <List
          onItemPress={(index, label) =>
            console.log('item pressed:', index, label)
          }
        />
        <Link
          label={'Copy all to clipboard'}
          onPress={() => console.log('link pressed: Copy all to clipboard')}
        />
        <Link
          label={'Send me a backup email'}
          onPress={() => console.log('link pressed: Send me a backup email')}
        />
      </ScrollView>
      <View style={styles.bottom}>
        <Button
          label={'DONE'}
          onPress={() => console.log('button pressed: DONE')}
        />
      </View>
    </SafeAreaView>
  );
};

export default App;
